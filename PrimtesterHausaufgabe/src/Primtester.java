
public class Primtester {

	public static boolean primtester(long zahl) {
		if(zahl < 2) {
			return false;
		}
		for(long i = 2;i < zahl;i++) {
			if(zahl%i == 0) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		Stoppuhr.start();
		System.out.println(primtester(999_999_929L));
		Stoppuhr.stopp();
		System.out.println(Stoppuhr.getDauer_Ms());
	}

}
