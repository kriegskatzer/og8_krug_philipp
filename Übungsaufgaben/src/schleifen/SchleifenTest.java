package schleifen;

import java.util.Scanner;

public class SchleifenTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int anzahl; 

		System.out.println("Bitte geben sie eine Zahl ein: ");
		anzahl = scan.nextInt();

		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");
		}
		
		for (int i = 0; i < anzahl; i++) {
			System.out.print("-");
		}
		
		int i = 0;
		do{
			System.out.print("*");
			i++;
		}while(i  < anzahl);
	}

}
