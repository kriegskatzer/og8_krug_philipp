
public class Person {
	private String name;
	private String telefonnummer;
	private boolean hatJahresbeitragBezahlt;
	
	
	public Person(String name, String telefonnummer, boolean hatJahresbeitragBezahlt) {
		super();
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.hatJahresbeitragBezahlt = hatJahresbeitragBezahlt;
	}
	

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getTelefonnummer() {
		return telefonnummer;
	}
	
	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}
	
	public boolean isHatJahresbeitragBezahlt() {
		return hatJahresbeitragBezahlt;
	}
	
	public void setHatJahresbeitragBezahlt(boolean hatJahresbeitragBezahlt) {
		this.hatJahresbeitragBezahlt = hatJahresbeitragBezahlt;
	}
}
